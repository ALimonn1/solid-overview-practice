package com.galvanize;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TodoTest {
    @Test
    public void toStringWorks() {
        Todo todo = new Todo("Do stuff", new TodoOwner("Alex", "Hamilton", "alex@example.com", "Treasurer"));

        assertEquals(
                "Do stuff <Alex Hamilton> alex@example.com (Treasurer): incomplete",
                todo.toString()
        );

        todo.markComplete();

        assertEquals(
                "Do stuff <Alex Hamilton> alex@example.com (Treasurer): complete",
                todo.toString()
        );
    }

    @Test
    public void itHasADescription() {
        Todo todo = new Todo("Do stuff", new TodoOwner("Alex", "Hamilton", "alex@example.com", "Treasurer"));
        todo.setDescription("There's a million things he hasn't done");

        assertEquals(
                "There's a million things he hasn't done",
                todo.getDescription()
        );
    }

}
