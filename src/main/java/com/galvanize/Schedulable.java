package com.galvanize;

import java.time.LocalDateTime;

public interface Schedulable {
    LocalDateTime getLocalDateTime();
}
