package com.galvanize;

import java.time.LocalDateTime;

public class Todo extends ICalendarItem implements Completable{

    private final String text;
    private String description;
    private TodoStatus status = TodoStatus.INCOMPLETE;
    private LocalDateTime completedAt;
    private TodoOwner owner;

    public Todo(String text, TodoOwner owner) {
        this.text = text;
        this.owner = owner;
    }

    public String getText() {
        return text;
    }

    @Override
    public String getTextToDisplay() {
        return getText();
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public void markComplete() {
        status = TodoStatus.COMPLETE;
        completedAt = LocalDateTime.now();
    }

    @Override
    public void markIncomplete() {
        status = TodoStatus.INCOMPLETE;
        completedAt = null;
    }

    @Override
    public boolean isComplete() {
        return status == TodoStatus.COMPLETE;
    }

    public LocalDateTime getCompletedAt() {
        return completedAt;
    }

    public TodoOwner getOwner() {
        return owner;
    }

    public void setOwner(TodoOwner owner) {
        this.owner = owner;
    }

    @Override
    public String iCalendar() {
        if (getTextToDisplay() == null) return "";

        return new StringBuilder()
                .append("BEGIN:VTODO\n")
                .append(String.format("COMPLETED::%s\n", getCompletedAt()))
                .append(String.format("UID:%s@example.com\n", this.getUuid()))
                .append(String.format("SUMMARY:%s\n", getTextToDisplay()))
                .append("END:VTODO\n")
                .toString();
    }

    @Override
    public String toString() {
        return String.format(
                "%s <%s %s> %s (%s): %s",
                getText(),
                this.owner.getFirstName(),
                this.owner.getLastName(),
                this.owner.getEmail(),
                this.owner.getJobTitle(),
                status == TodoStatus.INCOMPLETE ? "incomplete" : "complete"
        );
    }

}
