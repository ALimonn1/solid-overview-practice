package com.galvanize;

public interface Completable extends Exportable {

    void markComplete();

    void markIncomplete();

    boolean isComplete();
}
