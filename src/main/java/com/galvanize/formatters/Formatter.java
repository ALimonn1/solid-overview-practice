package com.galvanize.formatters;

import com.galvanize.Calendar;

public interface Formatter {
    String format(Calendar calendar);
}
