package com.galvanize;

import com.galvanize.formatters.Formatter;
import com.galvanize.formatters.MonthlyFormatter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class Calendar {

    private final List<Schedulable> schedulables = new ArrayList<>();

    public void addSchedulable(Schedulable schedulable) {
        schedulables.add(schedulable);
        schedulables.sort((a, b) -> {
            LocalDateTime dateA = a.getLocalDateTime();
            LocalDateTime dateB = b.getLocalDateTime();
            return dateA.compareTo(dateB);
        });
    }

    public List<Schedulable> items() {
        return schedulables;
    }

    public List<LocalDate> dates() {
        return schedulables
                .stream()
                .map(Schedulable::getLocalDateTime)
                .map(LocalDateTime::toLocalDate)
                .distinct()
                .collect(toList());
    }


    public List<String> descriptionsFor(LocalDate date) {
        return schedulables.stream()
                .filter(item -> item.getLocalDateTime().toLocalDate().equals(date))
                .map(Object::toString)
                .collect(toList());
    }

    public LocalDateTime getFirstDateTime() {
        if (schedulables.isEmpty()) return null;
        Schedulable item = schedulables.get(0);
        return item.getLocalDateTime();
    }

    public LocalDateTime getLastDateTime() {
        if (schedulables.isEmpty()) return null;
        Schedulable item = schedulables.size() == 1 ? schedulables.get(0) : schedulables.get(schedulables.size() - 1);
        return item.getLocalDateTime();
    }

    public String format(Formatter formatter) {
        return formatter.format(this);
    }
}
