package com.galvanize;

public interface Exportable {
    String iCalendar();

    String getTextToDisplay();

}
